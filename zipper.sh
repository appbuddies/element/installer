#!/usr/bin/env bash
wget https://github.com/tinytree-dk/element/archive/master.zip
unzip master.zip -d working
cd working/element-master
composer install
zip -ry ../../element-craft.zip .
cd ../..
mv element-craft.zip public/element-craft.zip
rm -rf working
rm master.zip
